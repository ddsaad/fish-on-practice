import React from "react"
import { View, Text, TouchableOpacity, ScrollView, Image } from "react-native"
import styles from "../styles/styles"

function HomePostHeader() {
  return (
    <View style={[styles.flexBetween, { flexDirection: "row" }]}>
      <View style={[styles.flexCenter, { flexDirection: "row", gap: 5 }]}>
        <Image
          style={{ width: 28, height: 28, borderRadius: 14 }}
          resizeMode="cover"
          source={require("../assets/images/pictures/home-post-profile.jpeg")}
        />
        <Text style={{ fontSize: 12, color: "#444444", fontWeight: "600" }}>
          Sarah Patrick
        </Text>
      </View>

      <Text style={{ fontSize: 12, color: "#808080" }}>July 24, 2023</Text>
    </View>
  )
}

export default HomePostHeader
