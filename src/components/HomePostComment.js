import React from "react"
import { View, Text, Image } from "react-native"
import HomePostHeader from "./HomePostHeader"

function HomePostComment() {
  return (
    <View>
      <HomePostHeader />
      <View
        style={{
          marginTop: 12,
          marginLeft: 14,
          paddingLeft: 14,
          borderLeftWidth: 1,
        }}
      >
        <Text>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
          nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
          volutpat. Ut wisi enim ad
        </Text>
        <View style={{ flexDirection: "row", gap: 8, marginTop: 10 }}>
          <Image
            resizeMode="contain"
            style={{ width: 18, height: 18, alignItems: "center" }}
            source={require("../assets/images/icons/heart-outline-grey.png")}
          />

          <Image
            resizeMode="contain"
            style={{ width: 18, height: 18 }}
            source={require("../assets/images/icons/message-grey.png")}
          />

          <Image
            resizeMode="contain"
            style={{ width: 18, height: 18 }}
            source={require("../assets/images/icons/share-grey.png")}
          />
        </View>
      </View>
    </View>
  )
}

export default HomePostComment
