import React from "react"
import { View, Text, Image, Pressable } from "react-native"
import styles from "../styles/styles"

function HomeFooter({ navigation }) {
  return (
    <View style={styles.homeFooter}>
      <Pressable style={styles.flexCenter}>
        <Image
          resizeMode="contain"
          style={{ width: 25, height: 25 }}
          source={require("../assets/images/icons/home-outline-grey.png")}
          onPress={() => navigation.navigate("Home")}
        />
        {/* <Text style={{ fontSize: 12, fontWeight: "bold", color: "#26AD59" }}>
          Home
        </Text> */}
        <Text style={{ fontSize: 12, color: "#666666" }}>Home</Text>
      </Pressable>

      <Pressable style={styles.flexCenter} onPress={() => navigation.navigate("Discover")}>
        <Image
          resizeMode="contain"
          style={{ width: 25, height: 25 }}
          source={require("../assets/images/icons/magnifying-glass-outline-grey.png")}
        />
        <Text style={{ fontSize: 12, color: "#666666" }}>Discover</Text>
      </Pressable>

      <Pressable style={styles.flexCenter} onPress={() => navigation.navigate("Watch")}>
        <Image
          resizeMode="contain"
          style={{ width: 25, height: 25 }}
          source={require("../assets/images/icons/camcorder-outline-grey.png")}
        />
        <Text style={{ fontSize: 12, color: "#666666" }}>Watch</Text>
      </Pressable>

      <Pressable style={styles.flexCenter} onPress={() => navigation.navigate("HomeMyAreaMap")}>
        <Image
          resizeMode="contain"
          style={{ width: 25, height: 25 }}
          source={require("../assets/images/icons/map-outline-grey.png")}
        />
        <Text style={{ fontSize: 12, color: "#666666" }}>Map</Text>
      </Pressable>

      <Pressable style={styles.flexCenter} onPress={() => navigation.navigate("Channels")}>
        <Image
          resizeMode="contain"
          style={{ width: 25, height: 25 }}
          source={require("../assets/images/icons/tv-outline-grey.png")}
        />
        <Text style={{ fontSize: 12, color: "#666666" }}>Channels</Text>
      </Pressable>
    </View>
  )
}

export default HomeFooter
