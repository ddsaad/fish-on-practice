import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, Image, Pressable } from "react-native";
import styles from "../styles/styles";
import HomePostHeader from "./HomePostHeader";
import HomePostComment from "./HomePostComment";

const HomePost = ({ navigation }) => {
  const [isBreezeBtnActive, setIsBreezeBtnActive] = useState(false);
  const [isCommentsOpen, setIsCommentsOpen] = useState(false);

  useEffect(() => {
    return () => {
      setIsBreezeBtnActive(false);
      setIsCommentsOpen(false);
    };
  }, []);

  return (
    <View>
      <HomePostHeader />

      <View
        style={[styles.flexBetween, { flexDirection: "row", marginTop: 5 }]}
      >
        <View style={[styles.flexCenter, { flexDirection: "row", gap: 5 }]}>
          <Image
            style={{ width: 20, height: 20 }}
            resizeMode="contain"
            source={require("../assets/images/icons/pin-location-outline-orange.png")}
          />
          <Text
            style={{
              fontSize: 11,
              color: "#45A7EA",
              borderBottomColor: "#45A7EA",
              borderBottomWidth: 1,
            }}
          >
            Roadside beach, Miami Coast - 9244450
          </Text>
        </View>

        <TouchableOpacity
          style={{
            backgroundColor: isBreezeBtnActive ? "#EAD6C0" : "#FF9D2F33",
            borderWidth: 1,
            borderColor: "#B87933",
            borderRadius: 10,
            paddingVertical: 3,
            paddingHorizontal: 8,
          }}
          onPress={() =>
            setIsBreezeBtnActive(
              (prevIsBreezeBtnValue) => !prevIsBreezeBtnValue
            )
          }
        >
          <Image
            resizeMode="contain"
            source={require("../assets/images/icons/breeze-orange.png")}
          />
        </TouchableOpacity>
      </View>
      <>
        {isBreezeBtnActive ? (
          <View>
            <Text style={{ color: "#5E5E5E", marginTop: 10 }}>
              Lorem ipsum dolor sit amet, consectetuer adipiscing el.
            </Text>
            <View
              style={{
                alignItems: "center",
                flexDirection: "row",
                gap: 8,
                marginTop: 10,
              }}
            >
              <Text style={{ fontSize: 12, fontWeight: 500, color: "#5E5E5E" }}>
                Bait Used
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  gap: 5,
                  flex: 1,
                  overflow: "auto",
                }}
              >
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
              </View>
            </View>

            <View
              style={{
                alignItems: "center",
                flexDirection: "row",
                gap: 8,
                marginTop: 10,
              }}
            >
              <Text style={{ fontSize: 12, fontWeight: 500, color: "#5E5E5E" }}>
                Technique Used
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  gap: 5,
                  flex: 1,
                  overflow: "auto",
                }}
              >
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
              </View>
            </View>

            <View
              style={{
                alignItems: "center",
                flexDirection: "row",
                gap: 8,
                marginTop: 10,
              }}
            >
              <Text style={{ fontSize: 12, fontWeight: 500, color: "#5E5E5E" }}>
                Recommendations
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  gap: 5,
                  flex: 1,
                  overflow: "auto",
                }}
              >
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
                <Text
                  style={{
                    backgroundColor: "#1B744B1A",
                    borderWidth: 1,
                    borderColor: "#21734399",
                    color: "#4E8F3E",
                    paddingVertical: 5,
                    paddingHorizontal: 10,
                    borderRadius: 20,
                    fontSize: 10,
                    flexShrink: 0,
                  }}
                >
                  Lorem Ipsum
                </Text>
              </View>
            </View>
          </View>
        ) : (
          <Text style={{ fontSize: 12, color: "#5E5E5E", marginTop: 10 }}>
            🎣 Cast, Hook, and Reel: My Epic Fishing Expedition! Join me as I
            share the highs, lows, and fish tales from my recent angling
            adventure. Great Experience!
          </Text>
        )}
      </>

      <Image
        style={{
          width: "100%",
          height: "auto",
          aspectRatio: isBreezeBtnActive ? 1.64 / 1 : 1.2 / 1,
          borderRadius: 10,
          marginTop: 10,
        }}
        resizeMode="cover"
        source={require("../assets/images/pictures/home-post-main.jpeg")}
      />

      <View
        style={[styles.flexBetween, { flexDirection: "row", marginTop: 10 }]}
      >
        <View style={[styles.flexCenter, { flexDirection: "row", gap: 20 }]}>
          <TouchableOpacity
            style={[styles.flexCenter, { flexDirection: "row", gap: 3 }]}
          >
            <Image
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
              source={require("../assets/images/icons/arrow-up-grey.png")}
            />
            <Text style={{ fontSize: 12, color: "#808080" }}>20</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              setIsCommentsOpen((prevIsCommentsOpen) => !prevIsCommentsOpen)
            }
            style={[styles.flexCenter, { flexDirection: "row", gap: 3 }]}
          >
            <Image
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
              source={require("../assets/images/icons/message-grey.png")}
            />
            <Text style={{ fontSize: 12, color: "#808080" }}>220</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.flexCenter, { flexDirection: "row", gap: 3 }]}
          >
            <Image
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
              source={require("../assets/images/icons/share-grey.png")}
            />
            <Text style={{ fontSize: 12, color: "#808080" }}>220</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.flexCenter, { flexDirection: "row", gap: 3 }]}
          >
            <Image
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
              source={require("../assets/images/icons/eye-grey.png")}
            />
            <Text style={{ fontSize: 12, color: "#808080" }}>829</Text>
          </TouchableOpacity>
        </View>

        <Pressable>
          <Image
            resizeMode="contain"
            style={{ width: 20, height: 20 }}
            source={require("../assets/images/icons/flag-outline-green.png")}
          />
        </Pressable>
      </View>

      <>
        {isCommentsOpen ? (
          <View style={{ marginTop: 15, gap: 15 }}>
            <HomePostComment />
            <HomePostComment />
          </View>
        ) : (
          ""
        )}
      </>
    </View>
  );
};

export default HomePost;
