import React from "react"
import { Text, Pressable } from "react-native"
import styles from "../styles/styles"

function AddPostUnusedBait({bait, updateBaitStatus}) {
  return (
    <Pressable
    onPress={() => updateBaitStatus(bait.id)}
      style={[
        styles.flexCenter,
        {
          flexDirection: "row",
          gap: 3,
          borderWidth: 1,
          borderColor: "#D5D5D5",
          borderRadius: 30,
          paddingHorizontal: 15,
          paddingVertical: 10,
        },
      ]}
    >
      <Text style={{ fontSize: 12, color: "#888888" }}>+</Text>
      <Text style={{ fontSize: 12, color: "#888888" }}>{bait.baitText}</Text>
    </Pressable>
  )
}

export default AddPostUnusedBait
