import React, { useState } from "react"
import { View, Image, Pressable } from "react-native"
import styles from "../styles/styles"
// import SideMenu from "./SideMenu"

function HomeHeader({navigation}) {
  const [showSideMenu, setShowSideMenu] = useState(false)

  return (
    <>
    <View style={styles.homeHeader}>
    
    {/* <Pressable onPress={() => setShowSideMenu((oldState) => !oldState)}> */}
    <Pressable onPress={() => navigation.openDrawer()}>
      <Image
        resizeMode="cover"
        style={{ width: 40, height: 40, borderRadius: 20 }}
        source={require("../assets/images/pictures/home-header-profile.jpeg")}
      />
      </Pressable>

      <Image
        resizeMode="contain"
        style={{ width: 100, height: 55 }}
        source={require("../assets/images/pictures/fish-on-dev-logo.png")}
      />

      <View style={[styles.flexCenter, { flexDirection: "row", gap: 10 }]}>
        <Image
          resizeMode="contain"
          style={{ width: 25 }}
          source={require("../assets/images/icons/notification-bell-outline-green.png")}
        />
        <Image
          resizeMode="contain"
          style={{ width: 25 }}
          source={require("../assets/images/icons/paper-plane-send-outline-green.png")}
        />
      </View>
    </View>

    {/* {(showSideMenu) ? <SideMenu /> : ""} */}
    </>
  )
}

export default HomeHeader
