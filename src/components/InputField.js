import styles from "../styles/styles"
import React from "react"
import {
  Button,
  Image,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from "react-native"

export default InputField = ({ label, placeholder, type }) => {
  return (
    <View style={styles.onboardingInputContainer}>
      <Text style={styles.onboardingInputLabel}>{label}</Text>
      <TextInput
        style={styles.onboardingInput}
        placeholderTextColor={"#888888"}
        placeholder={placeholder}
        secureTextEntry={type === "password" ? true : false}
      />
    </View>
  )
}
