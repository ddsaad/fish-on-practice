import React from "react"
import { Text, Pressable } from "react-native"
import styles from "../styles/styles"

function AddPostUsedBait({bait, updateBaitStatus}) {
  return (
    <Pressable
    onPress={() => updateBaitStatus(bait.id)}
      style={[
        styles.flexCenter,
        {
          flexDirection: "row",
          gap: 3,
          backgroundColor: "#1B744B1A",
          borderWidth: 1,
          borderColor: "#21734399",
          borderRadius: 30,
          paddingHorizontal: 15,
          paddingVertical: 10,
        },
      ]}
    >
      <Text style={{ fontSize: 12, color: "#4E8F3E" }}>+</Text>
      <Text style={{ fontSize: 12, color: "#4E8F3E" }}>{bait.baitText}</Text>
    </Pressable>
  )
}

export default AddPostUsedBait
