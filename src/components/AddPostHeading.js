import React from "react"
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
} from "react-native"
import styles from "../styles/styles"

function AddPostHeading({navigation, heading}) {
  return (
    <>
      <Pressable
        style={{
          flexDirection: "row",
          alignItems: "center",
          gap: 7,
          paddingVertical: 10,
        }}
        onPress={() => {
          console.log("navigation")
          navigation.goBack()
        }}
      >
        <Image
          style={{ width: 7, height: 15 }}
          resizeMode="contain"
          source={require("../assets/images/icons/right-angle.png")}
        />
        <Text style={{ fontSize: 16, color: "rgba(106, 106, 106, 1)" }}>
          Back
        </Text>
      </Pressable>

      <Text
        style={{
          fontSize: 20,
          fontWeight: 500,
          color: "#474747",
        }}
      >
        {heading}
      </Text>
    </>
  )
}

export default AddPostHeading
