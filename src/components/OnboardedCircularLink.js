import styles from "../styles/styles"
import React from "react"
import {
  Button,
  Image,
  StyleSheet,
  Text,
  Touchable,
  TouchableOpacity,
  View,
} from "react-native"

export default OnboardedCircularLink = ({ imgSrc }) => {
  return (
    <TouchableOpacity style={[styles.onboardedCircularLink, styles.flexCenter]}>
      <Image
        resizeMode="contain"
        style={{ width: 25,height: 25 }}
        source={imgSrc}
      />
    </TouchableOpacity>
  )
}
