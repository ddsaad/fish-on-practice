import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
} from "react-native"
import styles from "../styles/styles"
import HomeHeader from "../components/HomeHeader"
import AddPostHeading from "../components/AddPostHeading"

function PostAddPicture({ navigation }) {
  return (
    <>
      <StatusBar />
      <HomeHeader navigation={navigation} />
      <View style={{ position: "relative", flex: 1, paddingHorizontal: 15 }}>
        {/* <Pressable
          style={{
            flexDirection: "row",
            alignItems: "center",
            gap: 7,
            paddingVertical: 10,
          }}
          onPress={() => {
            console.log("navigation")
            navigation.goBack()
          }}
        >
          <Image
            style={{ width: 7, height: 15 }}
            resizeMode="contain"
            source={require("../assets/images/icons/right-angle.png")}
          />
          <Text style={{ fontSize: 16, color: "rgba(106, 106, 106, 1)" }}>
            Back
          </Text>
        </Pressable>

        <Text
          style={{
            fontSize: 20,
            fontWeight: 500,
            color: "#474747",
          }}
        >
          Upload Picture
        </Text> */}

        <AddPostHeading navigation={navigation} heading={"Upload Picture"} />

        <View
          style={[
            {
              backgroundColor: "rgba(38, 173, 89, 0.08)",
              borderWidth: 1,
              borderColor: "rgba(38, 173, 89, 0.7)",
              borderStyle: "dashed",
              borderRadius: 20,
              aspectRatio: 1,
              width: "100%",
              marginTop: 90,
            },
            styles.flexCenter,
          ]}
        >
          <Image
            style={{ width: 80, height: 80 }}
            resizeMode="contain"
            source={require("../assets/images/icons/upload-cloud-outline-green.png")}
          />
          <Text style={{ color: "#5E5E5E", fontSize: 14.5, fontWeight: 500 }}>
            Upload Pictures from your Library
          </Text>
        </View>

        <Pressable
          style={[
            styles.onBoardGreenBtn,
            {
              width: "100%",
              position: "absolute",
              left: 15,
              bottom: 25,
            },
          ]}
        >
          <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
            Add Details
          </Text>
        </Pressable>
      </View>
    </>
  )
}

export default PostAddPicture
