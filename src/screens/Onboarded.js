// src/screens/Detail.js
import React from "react"
import { Image, ScrollView, Text, TouchableOpacity, View, StatusBar } from "react-native"
import styles from "../styles/styles"
import OnboardedCircularLink from "../components/OnboardedCircularLink"

const Onboarded = ({ navigation, route }) => {
  return (
    <View style={{ backgroundColor: "white" }}>
      <StatusBar />
      <ScrollView>
        <Image
          style={{ width: "100%" }}
          source={require("../assets/images/pictures/onboarded-main.png")}
        />
        <View style={styles.container}>
          <Text style={[styles.onboardedTitle, styles.textAlignCenter]}>
            Let's get started
          </Text>
          <Text
            style={[
              styles.onboardedText,
              styles.textAlignCenter,
              { marginTop: 10 },
            ]}
          >
            Everything is started from here
          </Text>

          <TouchableOpacity
            style={[styles.onBoardGreenBtn, { marginTop: 20 }]}
            onPress={() => navigation.navigate("Signin")}
          >
            <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
              Sign In
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.onBoardGreenOutlineBtn, { marginTop: 15 }]}
            onPress={() => navigation.navigate("Signup")}
          >
            <Text
              style={{ fontWeight: 500, textAlign: "center", color: "#26AD59" }}
            >
              Sign Up
            </Text>
          </TouchableOpacity>

          <View style={[styles.seperatorLine, { marginTop: 40 }]}>
            <Text style={styles.seperatorLineText}>or</Text>
          </View>

          <View
            style={[
              styles.flexCenter,
              { flexDirection: "row", gap: 30, marginTop: 35 },
            ]}
          >
            <OnboardedCircularLink
              imgSrc={require("../assets/images/icons/gmail-logo.png")}
            />
            <OnboardedCircularLink
              imgSrc={require("../assets/images/icons/facebook-logo.png")}
            />
            <OnboardedCircularLink
              imgSrc={require("../assets/images/icons/apple-logo.png")}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

export default Onboarded
