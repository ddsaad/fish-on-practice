import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
  TextInput,
} from "react-native"
import styles from "../styles/styles"
import HomeHeader from "../components/HomeHeader"
import AddPostHeading from "../components/AddPostHeading"
import AddPostUnusedBait from "../components/AddPostUnusedBait"
import AddPostUsedBait from "../components/AddPostUsedBait"

export default function PostAddBaitsUsed({ navigation }) {
  const [baits, setBaits] = useState([
    { id: 1, baitText: "init amet", status: true },
    { id: 2, baitText: "init", status: true },
    { id: 3, baitText: "amet", status: true },
    { id: 4, baitText: "lorem", status: true },
    { id: 5, baitText: "ipsum", status: true },
    { id: 6, baitText: "lorem ipsum", status: false },
    { id: 7, baitText: "hello", status: false },
    { id: 8, baitText: "worl", status: false },
    { id: 9, baitText: "world", status: false },
    { id: 10, baitText: "sit", status: false },
    { id: 11, baitText: "init amet", status: false },
    { id: 12, baitText: "init", status: false },
    { id: 13, baitText: "amet", status: false },
    { id: 14, baitText: "lorem", status: false },
    { id: 15, baitText: "ipsum", status: false },
    { id: 16, baitText: "lorem ipsum", status: false },
    { id: 17, baitText: "hello", status: false },
    { id: 18, baitText: "worl", status: false },
    { id: 19, baitText: "world", status: false },
    { id: 20, baitText: "sit", status: false },
    { id: 21, baitText: "init amet", status: false },
    { id: 22, baitText: "init", status: false },
    { id: 23, baitText: "amet", status: false },
    { id: 24, baitText: "lorem", status: false },
    { id: 25, baitText: "ipsum", status: false },
    { id: 26, baitText: "lorem ipsum", status: false },
    { id: 27, baitText: "hello", status: false },
    { id: 28, baitText: "worl", status: false },
    { id: 29, baitText: "world", status: false },
    { id: 30, baitText: "sit", status: false },
  ])

  const updateBaitStatus = (id) => {
    setBaits((prevBaits) => {
      return prevBaits.map((bait) =>
        bait.id === id ? { ...bait, status: !bait.status } : bait
      )
    })
  }

  return (
    <>
      <StatusBar />
      <HomeHeader navigation={navigation} />
      <View
        style={{
          position: "relative",
          flex: 1,
        }}
      >
        <View style={{ paddingHorizontal: 15, flex: 1 }}>
          <AddPostHeading navigation={navigation} heading={"Add Baits Used"} />

          <View style={{ position: "relative", marginTop: 20 }}>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#D5D5D5",
                borderRadius: 40,
                fontSize: 12,
                paddingVertical: 10,
                paddingLeft: 40,
                paddingRight: 10,
              }}
              placeholder="Search"
            />
            <Image
              resizeMode="contain"
              style={{ width: 20, position: "absolute", left: 12 }}
              source={require("../assets/images/icons/magnifying-search-grey.png")}
            />
          </View>
          {baits.filter((bait) => bait.status === true).length > 0 ? (
            <>
              <Text
                style={{ fontSize: 12, color: "#5E5E5E", paddingVertical: 10 }}
              >
                Baits Used
              </Text>
              <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    flexWrap: "wrap",
                    gap: 10,
                    minHeight: 80,
                  }}
                >
                  {baits
                    .filter((bait) => bait.status)
                    .map((bait) => (
                      <AddPostUsedBait
                        key={bait.id}
                        bait={bait}
                        updateBaitStatus={updateBaitStatus}
                      />
                    ))}
                </View>
              </ScrollView>
            </>
          ) : (
            ""
          )}

          {baits.filter((bait) => bait.status === false).length > 0 ? (
            <>
              <Text
                style={{ fontSize: 12, color: "#5E5E5E", paddingVertical: 10 }}
              >
                Click to add suggestions
              </Text>
              <ScrollView
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    flexWrap: "wrap",
                    gap: 10,
                    minHeight: 80,
                  }}
                >
                  {baits
                    .filter((bait) => bait.status === false)
                    .map((bait) => (
                      <AddPostUnusedBait
                        key={bait.id}
                        bait={bait}
                        updateBaitStatus={updateBaitStatus}
                      />
                    ))}
                </View>
              </ScrollView>
            </>
          ) : (
            ""
          )}

          <Pressable
            style={[
              styles.onBoardGreenBtn,
              {
                width: "100%",
                marginVertical: 25,
              },
            ]}
          >
            <Text style={[styles.onBoardGreenBtnText]}>Continue</Text>
          </Pressable>
        </View>
      </View>
    </>
  )
}
