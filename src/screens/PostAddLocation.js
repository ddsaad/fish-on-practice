import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
  TextInput,
} from "react-native"
import MapView, { Marker } from "react-native-maps"
import styles from "../styles/styles"
import HomeHeader from "../components/HomeHeader"
import AddPostHeading from "../components/AddPostHeading"

function PostAddDetails({ navigation }) {
  const [mapRegion, setMapRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  })

  const handleMapPress = (event) => {
    const { coordinate } = event.nativeEvent
    console.log(coordinate)
    setMapRegion((oldMapRegion) => ({
      ...oldMapRegion,
      latitude: coordinate.latitude,
      longitude: coordinate.longitude,
    }))
  }

  return (
    <>
      <StatusBar />
      <HomeHeader navigation={navigation} />
      <View
        style={{
          position: "relative",
          flex: 1,
        }}
      >
        <View style={{ paddingHorizontal: 15 }}>
          <AddPostHeading navigation={navigation} heading={"Add Location"} />

          <View style={{ position: "relative", marginTop: 20 }}>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#D5D5D5",
                borderRadius: 40,
                fontSize: 12,
                paddingVertical: 10,
                paddingLeft: 40,
                paddingRight: 10,
              }}
              placeholder="Search"
            />
            <Image
              resizeMode="contain"
              style={{ width: 20, position: "absolute", left: 12 }}
              source={require("../assets/images/icons/magnifying-search-grey.png")}
            />
          </View>
        </View>

        <View style={{ marginTop: 30 }}>
          <MapView
            style={{ alignSelf: "stretch", height: "80%" }}
            region={mapRegion}
            onLongPress={handleMapPress}
          >
            <Marker
              coordinate={{
                latitude: mapRegion.latitude,
                longitude: mapRegion.longitude,
              }}
              title="Marker"
            />
          </MapView>
        </View>

        <View
          style={{
            position: "absolute",
            left: 0,
            bottom: 10,
            width: "100%",
            paddingHorizontal: 15,
          }}
        >
          <Pressable style={[styles.onBoardGreenBtn]}>
            <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
              Continue
            </Text>
          </Pressable>
        </View>
      </View>
    </>
  )
}
export default PostAddDetails
