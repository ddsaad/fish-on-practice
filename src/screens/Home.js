import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
} from "react-native"
import styles from "../styles/styles"
import HomePost from "../components/HomePost"
import HomeHeader from "../components/HomeHeader"
import HomeFooter from "../components/HomeFooter"

const Home = ({ route, navigation }) => {
  const [activeHomeTab, setActiveHomeTab] = useState("for-you")

  return (
    <View
      style={{
        position: "relative",
        minHeight: "100%",
        flex: 1,
      }}
    >
      <StatusBar />
      <HomeHeader navigation={navigation} />

      <View
        style={{
          paddingHorizontal: 15,
          flexDirection: "row",
          backgroundColor: "white",
        }}
      >
        <TouchableOpacity
          onPress={() => setActiveHomeTab("for-you")}
          style={[
            styles.flexCenter,
            styles.homeTab,
            {
              borderBottomColor:
                activeHomeTab === "for-you" ? "#26AD59" : "#D2D2D2",
            },
          ]}
        >
          <Text
            style={{
              color: activeHomeTab === "for-you" ? "#26AD59" : "#6A6A6A",
            }}
          >
            For You
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveHomeTab("following")}
          style={[
            styles.flexCenter,
            styles.homeTab,
            {
              borderBottomColor:
                activeHomeTab === "following" ? "#26AD59" : "#D2D2D2",
            },
          ]}
        >
          <Text
            style={{
              color: activeHomeTab === "following" ? "#26AD59" : "#6A6A6A",
            }}
          >
            Following
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveHomeTab("my-area")}
          style={[
            styles.flexCenter,
            styles.homeTab,
            {
              borderBottomColor:
                activeHomeTab === "my-area" ? "#26AD59" : "#D2D2D2",
            },
          ]}
        >
          <Text
            style={{
              color: activeHomeTab === "my-area" ? "#26AD59" : "#6A6A6A",
            }}
          >
            My Area
          </Text>
        </TouchableOpacity>
      </View>

      {/* USING flex: 1 on parent view of scrollview lets us use scrollview inside a view */}
      <View style={{ position: "relative", flex: 1 }}>
        <ScrollView nestedScrollEnabled={true}>
          <View
            style={{
              padding: 15,
              backgroundColor: "white",
              flex: 1,
              paddingBottom: 85,
            }}
          >
            {activeHomeTab === "for-you" ? (
              <View style={{ gap: 30, flex: 1 }}>
                {[...Array(2)].map((_, i) => (
                  <HomePost key={`${activeHomeTab}-${i}`} />
                ))}
              </View>
            ) : activeHomeTab === "following" ? (
              <View style={{ gap: 30 }}>
                {[...Array(3)].map((_, i) => (
                  <HomePost key={`${activeHomeTab}-${i}`} />
                ))}
              </View>
            ) : activeHomeTab === "my-area" ? (
              <View>
                <View
                  style={{
                    alignItems: "flex-start",
                    borderWidth: 1,
                    borderColor: "#D5D5D5",
                    borderRadius: 10,
                    padding: 15,
                    marginBottom: 15,
                  }}
                >
                  <Text
                    style={{
                      color: "#474747",
                      fontWeight: 500,
                      marginBottom: 5,
                    }}
                  >
                    No area selected
                  </Text>
                  <Text
                    style={{ color: "#5E5E5E", marginBottom: 15, fontSize: 12 }}
                  >
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                    sed diam{" "}
                  </Text>
                  <Pressable
                    style={{
                      backgroundColor: "#26AD59",
                      paddingHorizontal: 13,
                      paddingVertical: 7,
                      borderRadius: 10,
                    }}
                    onPress={() => navigation.navigate("HomeMyAreaMap")}
                  >
                    <Text style={{ color: "white", fontSize: 10 }}>
                      Select Area
                    </Text>
                  </Pressable>
                </View>
                <Text
                  style={{
                    fontSize: 15,
                    color: "#6A6A6A",
                    fontWeight: 600,
                    marginBottom: 15,
                  }}
                >
                  Recent posts from the area
                </Text>
                <View style={{ gap: 30 }}>
                  {[...Array(3)].map((_, i) => (
                    <HomePost key={`${activeHomeTab}-${i}`} />
                  ))}
                </View>
              </View>
            ) : (
              ""
            )}
          </View>
        </ScrollView>
        <Pressable
          style={{
            padding: 12,
            backgroundColor: "#26AD59",
            borderRadius: 29,
            alignSelf: "flex-end",
            position: "absolute",
            right: 15,
            bottom: 90,
          }}
          onPress={() => navigation.navigate("AddPost")}
          // onPress={() => navigation.openDrawer()}
        >
          <Image
            resizeMode="contain"
            style={{ width: 32, height: 32 }}
            source={require("../assets/images/icons/plus-white.png")}
          />
        </Pressable>
      </View>

      <HomeFooter navigation={navigation} />
    </View>
  )
}

export default Home
