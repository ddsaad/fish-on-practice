import React, { useState } from "react"
import {
  Pressable,
  SafeAreaView,
  StatusBar,
  Text,
  View,
  Image,
} from "react-native"
import styles from "../styles/styles"
import MapView, { Marker } from "react-native-maps"
import HomeHeader from "../components/HomeHeader"

function HomeMyAreaMap({ route, navigation }) {

  const [mapRegion, setMapRegion] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  })

  const handleMapPress = (event) => {
    const { coordinate } = event.nativeEvent
    console.log(coordinate)
    setMapRegion((oldMapRegion) => ({
      ...oldMapRegion,
      latitude: coordinate.latitude,
      longitude: coordinate.longitude,
    }))
  }

  return (
    <View style={{ backgroundColor: "white", position: "relative" }}>
      <StatusBar />
      <HomeHeader navigation={navigation} />
      <View style={{ paddingHorizontal: 15, marginVertical: 10, gap: 10 }}>
        <Pressable
          style={{ flexDirection: "row", alignItems: "center", gap: 7 }}
          onPress={() => {
            console.log("navigation")
            navigation.goBack()
          }}
        >
          <Image
            style={{ width: 7, height: 15 }}
            resizeMode="contain"
            source={require("../assets/images/icons/right-angle.png")}
          />
          <Text style={{ fontSize: 16, color: "rgba(106, 106, 106, 1)" }}>
            Back
          </Text>
        </Pressable>

        <Text
          style={{
            fontSize: 20,
            fontWeight: 500,
            color: "#474747",
          }}
        >
          My Area
        </Text>
      </View>

      <View>
        <MapView
          style={{ alignSelf: "stretch", height: "90%" }}
          region={mapRegion}
          onLongPress={handleMapPress}
        >
          <Marker
            coordinate={{
              latitude: mapRegion.latitude,
              longitude: mapRegion.longitude,
            }}
            title="Marker"
          />
        </MapView>
      </View>

      <Pressable
        style={[
          styles.onBoardGreenBtn,
          {
            marginTop: 20,
            width: "90%",
            position: "absolute",
            bottom: 110,
            left: "5%",
          },
        ]}
      >
        <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
          Save
        </Text>
      </Pressable>
    </View>
  )
}

export default HomeMyAreaMap
