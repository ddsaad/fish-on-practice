import React from "react"
import { View, Text, TouchableOpacity, ScrollView, StatusBar } from "react-native"
import styles from "../styles/styles"
import InputField from "../components/InputField"

const Signin = ({ navigation }) => {
  const [text, setText] = React.useState("")

  const handleInputChange = (input) => {
    setText(input)
  }

  return (
    // <SafeAreaView>
    <ScrollView>
      <View style={[styles.container, { flex: 1 }]}>
        <StatusBar />
        <Text style={{ color: "#474747", fontSize: 25, marginBottom: 25 }}>
          Welcome Back
        </Text>
        <View style={{ display: "flex", gap: 10 }}>
          <InputField
            label="Email or Username"
            placeholder="alexjohanson"
            type="text"
          />
          <InputField
            label="Password"
            placeholder="Enter your password"
            type="password"
          />
        </View>

        <TouchableOpacity
          style={{ alignSelf: "flex-end", marginTop: 10 }}
          onPress={() => console.log("HEYA")}
        >
          <Text
            style={{
              borderBottomWidth: 1,
              borderColor: "#727272",
              color: "#727272",
              fontSize: 10,
            }}
          >
            Forgot Password?
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style={[styles.onBoardGreenBtn, { marginTop: 20 }]}>
          <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
            Sign In
          </Text>
        </TouchableOpacity>

        <View style={[styles.seperatorLine, { marginTop: 40 }]}>
          <Text style={styles.seperatorLineText}>OR</Text>
        </View>
        <View
          style={[
            styles.flexCenter,
            { flexDirection: "row", gap: 30, marginTop: 35 },
          ]}
        >
          <OnboardedCircularLink
            imgSrc={require("../assets/images/icons/google-logo.png")}
          />
          <OnboardedCircularLink
            imgSrc={require("../assets/images/icons/facebook-logo.png")}
          />
          <OnboardedCircularLink
            imgSrc={require("../assets/images/icons/apple-logo.png")}
          />
        </View>

        <Text
          style={{
            textAlign: "center",
            color: "#5E5E5E",
            fontSize: 12,
            marginTop: 35,
          }}
        >
          Don't have an account?{" "}
          <Text
            style={{ fontWeight: 500, color: "#26AD59" }}
            onPress={() => navigation.navigate("Signup")}
          >
            Signup
          </Text>
        </Text>
      </View>
    </ScrollView>
    // </SafeAreaView>
  )
}

export default Signin
