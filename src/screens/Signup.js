import React from "react"
import { View, Text, TouchableOpacity, ScrollView, StatusBar } from "react-native"
import styles from "../styles/styles"

function Signup({ navigation }) {
  return (
    <ScrollView>
      <View style={styles.container}>
      <StatusBar />
        <Text style={{ color: "#474747", fontSize: 25, marginBottom: 25 }}>
          Create An Account
        </Text>
        <View style={{ display: "flex", gap: 10 }}>
          <InputField
            label="Full Name"
            placeholder="Alex Johanson"
            type="text"
          />
          <InputField
            label="Email Address"
            placeholder="alexjohanson@example.com"
            type="text"
          />
          <InputField label="Bio" placeholder="Fishing 101" type="text" />
          <InputField
            label="Years of Fishing Experience"
            placeholder="10"
            type="text"
          />
          <InputField label="Password" placeholder="" type="password" />
        </View>

        <TouchableOpacity style={[styles.onBoardGreenBtn, { marginTop: 70 }]}>
          <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
            Create an Account
          </Text>
        </TouchableOpacity>

        <Text
          style={{
            textAlign: "center",
            color: "#5E5E5E",
            fontSize: 12,
            marginTop: 35,
          }}
        >
          Already have an account?{" "}
          <Text
            style={{ fontWeight: 500, color: "#26AD59" }}
            onPress={() => navigation.navigate("Signin")}
          >
            Login
          </Text>
        </Text>
      </View>
    </ScrollView>
  )
}

export default Signup
