import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  StatusBar,
  TextInput,
} from "react-native";
import styles from "../styles/styles";
import HomeHeader from "../components/HomeHeader";
import AddPostHeading from "../components/AddPostHeading";

function PostAddDetails({ navigation }) {
  return (
    <>
      <StatusBar />
      <HomeHeader navigation={navigation} />
      <View style={{ position: "relative", flex: 1, paddingHorizontal: 15 }}>
        <AddPostHeading navigation={navigation} heading={"Add Details"} />

        <View
          style={{
            marginTop: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "flex-start",
          }}
        >
          <Text style={{ marginTop: 10, fontSize: 12, color: "#474747" }}>
            Tell your fishing experience..
          </Text>
          <Image
            style={{ width: 100, aspectRatio: 1, borderRadius: 12 }}
            resizeMode="cover"
            source={require("../assets/images/pictures/add-post-add-details.jpeg")}
          />
        </View>

        <View style={{ marginTop: 15 }}>
          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderTopWidth: 1,
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>Add location</Text>
            <Image
              style={{ width: 10, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/left-angle-grey.png")}
            />
          </Pressable>

          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>
              Add date of catch
            </Text>
            <Image
              style={{ width: 18, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/calender-outline-grey.png")}
            />
          </Pressable>

          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>
              Add bait used
            </Text>
            <Image
              style={{ width: 10, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/left-angle-grey.png")}
            />
          </Pressable>

          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>
              Add technique used
            </Text>
            <Image
              style={{ width: 10, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/left-angle-grey.png")}
            />
          </Pressable>

          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>
              Weather conditions
            </Text>
            <Image
              style={{ width: 10, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/left-angle-grey.png")}
            />
          </Pressable>

          <Pressable
            style={[
              styles.flexBetween,
              {
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#2173434D",
                paddingVertical: 8,
              },
            ]}
          >
            <Text style={{ color: "#5E5E5E", fontSize: 12 }}>
              Add sponsorship label
            </Text>
            <Image
              style={{ width: 10, height: 17 }}
              resizeMode="contain"
              source={require("../assets/images/icons/left-angle-grey.png")}
            />
          </Pressable>

          <Pressable style={{}}>
            <Text style={{ color: "#5E5E5E", marginVertical: 8, fontSize: 12 }}>
              Add recommendations
            </Text>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: "#D5D5D5",
                borderRadius: 20,
                textAlignVertical: "top",
                fontSize: 12,
                padding: 10,
              }}
              multiline={true}
              numberOfLines={5}
              placeholder="Type your recommendations here..."
            />
          </Pressable>
        </View>

        <Pressable
          style={[
            styles.onBoardGreenBtn,
            {
              width: "100%",
              position: "absolute",
              left: 15,
              bottom: 10,
            },
          ]}
        >
          <Text style={[styles.onBoardGreenBtnText, { fontWeight: 500 }]}>
            Create Post
          </Text>
        </Pressable>
      </View>
    </>
  );
}

export default PostAddDetails;
