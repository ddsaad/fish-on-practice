import React from "react"
import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createNativeStackNavigator } from "@react-navigation/native-stack"

import Home from "../screens/Home"
import Watch from "../screens/Watch"
import Discover from "../screens/Discover"
import Channels from "../screens/Channels"
import HomeMyAreaMap from "../screens/HomeMyAreaMap"

const BottomTabNavigatorStack = createNativeStackNavigator()
// const BottomTabNavigatorStack = createBottomTabNavigator()

const BottomTabNavigator = () => {
  return (
    <BottomTabNavigatorStack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName="Home"
    >
      <BottomTabNavigatorStack.Screen name="Home" component={Home} />
      <BottomTabNavigatorStack.Screen name="Discover" component={Discover} />
      <BottomTabNavigatorStack.Screen name="Watch" component={Watch} />
      <BottomTabNavigatorStack.Screen name="HomeMyAreaMap" component={HomeMyAreaMap} />
      <BottomTabNavigatorStack.Screen name="Channels" component={Channels} />
    </BottomTabNavigatorStack.Navigator>
  )
}

export default BottomTabNavigator
