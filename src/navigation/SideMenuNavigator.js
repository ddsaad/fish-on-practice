import "react-native-gesture-handler";
import React, { useState } from "react";
import { View, Text, Pressable, Image } from "react-native";
import BottomTabNavigator from "./BottomTabNavigator";
import AddPostNavigator from "./AddPostNavigator";
import {
  DrawerItemList,
  createDrawerNavigator,
} from "@react-navigation/drawer";
import { ScrollView } from "react-native-gesture-handler";
import ToggleSwitch from "toggle-switch-react-native";
import { useNavigation } from '@react-navigation/native';
// import Signin from "../screens/Signin"


const Drawer = createDrawerNavigator();

export default function SideMenuNavigator(props) {
  const { navigation } = props;
  // const navigation = useNavigation();
  const [isDarkMode, setIsDarkMode] = useState(false);
  console.log("object", navigation);

  return (
    <Drawer.Navigator
      initialRouteName="BottomTab"
      screenOptions={{
        drawerStyle: {
          width: "75%",
        },
      }}
      drawerContent={(props) => {
        return (
          <View style={{ paddingHorizontal: 20, flex: 1 }}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                gap: 10,
                marginTop: 30,
                marginBottom: 40,
                paddingBottom: 25,
                borderBottomWidth: 1,
                borderBottomColor: "#D2D2D2",
              }}
            >
              <Pressable>
                <Image
                  resizeMode="cover"
                  style={{ width: 45, height: 45, borderRadius: 22.5 }}
                  source={require("../assets/images/pictures/home-header-profile.jpeg")}
                />
              </Pressable>
              <View>
                <Text
                  style={{ fontSize: 18, fontWeight: 500, color: "#474747" }}
                >
                  Selene Patrik
                </Text>
                <Text
                  style={{ fontSize: 10, fontWeight: 500, color: "#5E5E5E" }}
                >
                  selene.patrik@gmail.com
                </Text>
              </View>
            </View>

            <ScrollView showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexShrink: 0,
                  flexGrow: 1,
                  gap: 40,
                  marginBottom: 40,
                }}
              >
                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}

                  onPress={() => navigation.navigate("AddPost", {screen: "PostAddDetails"})}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/profile-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Profile
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/settings-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Settings
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/community-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Communities
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/flag-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Flagged
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/bait-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Bait & Tackle
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/log-book-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Log Book
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/fish-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    My Circle
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/arrow-up-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Posts Liked
                  </Text>
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/money-sack-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Subscription Plan
                  </Text>

                  <Image
                    resizeMode="contain"
                    style={{
                      width: 50,
                      height: 30,
                      position: "absolute",
                      right: 0,
                    }}
                    source={require("../assets/images/pictures/free-badge-blue.png")}
                  />
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 25, height: 25 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/moon-outline-grey.png")}
                  />
                  <Text style={{ color: "#6A6A6A", fontSize: 16 }}>
                    Dark Mode
                  </Text>

                  <ToggleSwitch
                    isOn={isDarkMode}
                    onColor="green"
                    offColor="#6A6A6A"
                    size="small"
                    animationSpeed={200}
                    style={{position: "absolute", right: 0}}
                    onToggle={(isOn) =>
                      setIsDarkMode((prevIsDarkMode) => !prevIsDarkMode)
                    }
                  />
                </Pressable>

                <Pressable
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    gap: 25,
                  }}
                >
                  <Image
                    style={{ width: 30, height: 30 }}
                    resizeMode="contain"
                    source={require("../assets/images/icons/logout-outline-red.png")}
                  />
                  <Text style={{ color: "#F01429", fontSize: 16 }}>Logout</Text>
                </Pressable>
              </View>
            </ScrollView>
          </View>
          // <CustomSideMenuDrawer {...props} isDarkMode={isDarkMode} setIsDarkMode={setIsDarkMode} />
        );
      }}
    >
      <Drawer.Screen
        name="BottomTab"
        component={BottomTabNavigator}
        options={{ headerShown: false }}
      />

      <Drawer.Screen
        name="AddPost"
        component={AddPostNavigator}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  );
}