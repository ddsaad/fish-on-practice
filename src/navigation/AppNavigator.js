import "react-native-gesture-handler"

import React from "react"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"

import AuthNavigator from "./AuthNavigator"
// import BottomTabNavigator from "./BottomTabNavigator"
// import AddPostNavigator from "./AddPostNavigator"
import SideMenuNavigator from "./SideMenuNavigator"

const Drawer = createDrawerNavigator()
const AppNavigatorStack = createNativeStackNavigator()

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <AppNavigatorStack.Navigator initialRouteName="drawer">
        <AppNavigatorStack.Screen name="Auth" component={AuthNavigator} />
        <AppNavigatorStack.Screen name="drawer" options={{ headerShown: false }} component={SideMenuNavigator} />

        {/* <AppNavigatorStack.Screen
          name="BottomTab"
          component={BottomTabNavigator}
          options={{ headerShown: false }}
        />

        <AppNavigatorStack.Screen
          name="AddPost"
          component={AddPostNavigator}
          options={{ headerShown: false }}
        /> */}

      </AppNavigatorStack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigator
