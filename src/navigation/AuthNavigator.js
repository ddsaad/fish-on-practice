// src/navigation/AuthNavigator.js
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Onboarded from "../screens/Onboarded";
import Signin from "../screens/Signin";
import Signup from "../screens/Signup";

const AuthNavigatorStack = createNativeStackNavigator();

const AuthNavigator = () => {
  return (
    <AuthNavigatorStack.Navigator
      initialRouteName="Onboarded"
      screenOptions={{ headerShown: false }}
    >
      <AuthNavigatorStack.Screen name="Onboarded" component={Onboarded} />
      <AuthNavigatorStack.Screen name="Signin" component={Signin} />
      <AuthNavigatorStack.Screen name="Signup" component={Signup} />
    </AuthNavigatorStack.Navigator>
  );
};

export default AuthNavigator;
