// src/navigation/AuthNavigator.js
import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import PostAddPicture from "../screens/PostAddPicture";
import PostAddDetails from "../screens/PostAddDetails";
import PostAddLocation from "../screens/PostAddLocation";
import PostAddBaitsUsed from "../screens/PostAddBaitsUsed";

const AddPostNavigatorStack = createNativeStackNavigator();

const AddPostNavigator = () => {
  return (
    <AddPostNavigatorStack.Navigator
      // initialRouteName="PostAddBaitsUsed"
      screenOptions={{ headerShown: false }}
    >
      <AddPostNavigatorStack.Screen
        name="PostAddPicture"
        component={PostAddPicture}
      />
      <AddPostNavigatorStack.Screen
        name="PostAddDetails"
        component={PostAddDetails}
      />
      <AddPostNavigatorStack.Screen
        name="PostAddLocation"
        component={PostAddLocation}
      />
      <AddPostNavigatorStack.Screen
        name="PostAddBaitsUsed"
        component={PostAddBaitsUsed}
      />
    </AddPostNavigatorStack.Navigator>
  );
};

export default AddPostNavigator;
